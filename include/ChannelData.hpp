// ============================================================================
//
// = CONTEXT
//    ChannelData
//
// = File
//    ChannelData.hpp
//
// = AUTHOR
//    X. Elattaoui Synchrotron Soleil France
//
// ============================================================================

#ifndef _CHANNEL_DATA_H
#define _CHANNEL_DATA_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <vector>
#include <string>

/**
*  ChannelData class description:
*    This class handles a specific channel data.
*/

// ============================================================================
// class: ChannelData
// ============================================================================
class ChannelData 
{
public:
  //- ctor ---------------------------------
  ChannelData(std::string chName) : channel_name_(chName) 
  {
    reset(chName);
  }
  
  //- reset : initialize members
  void reset(std::string channelName)
  {
    channel_name_		= channelName;
    trigger_time_       = "NaN";
    wave_array_count_   = 0;
    vertical_offset_    = 0.;
    vertical_gain_      = 0.;
    horizontal_offset_  = 0.;
    horizontal_interval_= 0.;
    nominal_bits_       = 0;
    wave_array_1_       = 0;
    wave_array_2_       = 0;
    vertical_scaled_data_.clear();
    raw_waveform_data_.clear();
  }

  //- operator=
  ChannelData& operator=(const ChannelData& src)
  {
    if(&src != this)
    {
      this->channel_name_         = src.channel_name_;
      this->trigger_time_         = src.trigger_time_;
      this->wave_array_count_     = src.wave_array_count_;
      this->vertical_offset_      = src.vertical_offset_;
      this->vertical_gain_        = src.vertical_gain_;
      this->horizontal_offset_    = src.horizontal_offset_;
      this->horizontal_interval_  = src.horizontal_interval_;
      this->nominal_bits_         = src.nominal_bits_;
      this->wave_array_1_         = src.wave_array_1_;
      this->wave_array_2_         = src.wave_array_2_;
      this->vertical_scaled_data_ = src.vertical_scaled_data_;
      this->raw_waveform_data_    = src.raw_waveform_data_;
    }
    return *this;
  }
  
  std::string channel_name_;
  
  std::string trigger_time_;
  
  long wave_array_count_;
  
  double vertical_offset_;
  double vertical_gain_;
  
  double horizontal_offset_;
  double horizontal_interval_;
  
  short nominal_bits_;
  long wave_array_1_;
  long wave_array_2_;
  
  std::vector < double > vertical_scaled_data_;
  std::vector < short >  raw_waveform_data_;

};

#endif //- _CHANNEL_DATA_H
