//*******************************************************************************
//* Copyright (c) 2008-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
//******************************************************************************************
//
//
//			september 16, 2004 :  Header file of the WaveForm Class
//
//		it contains the waveform raw data, at least there will be scaled
//
//		author : X.Elattaoui
//
//******************************************************************************************

#ifndef _WaveForm_data_H
#define _WaveForm_data_H

#include <string>
#include "WaveformException.h"
#include "ChannelData.hpp"
#include "SocketLecroy.h"

const long MAX_WAVEFORM_DATA_LENGTH = 1500000;

/*************************************************************************************************************************************************/
/*************************************************************************************************************************************************/

//- IMPORTANT NOTE:
//-		In the Lecroy documentation "Remote Control Manual", its explain that the structure begins at the offset 21 of the received block.
//-		But the tests done with two Lecroy scopes (a WR6100 and WP7100) have shown that this offset can be 21 or 15.
//-		For the moment, this offset is 15.
//-		Now this offset is dynamically calculated.

/*************************************************************************************************************************************************/
/*************************************************************************************************************************************************/

//- alignement in memory in bytes ( by default : alignement on 32 bits
#pragma pack(1)

//- Structure to store the acquired data
typedef struct
{
  char	descriptor_name [16];   //- descriptor name (always begin with WAVEDESC string)
  char	template_name	[16];
  short	comm_type;              //- 0 = BYTE ; 1 = WORD  format
  short	comm_order;             //- 0 = HIFIRST ; 1 = LOFIRST
//- Blocks :
  long	wave_descriptor;        //- length (in bytes) of block WAVEDESC
  long	user_text;              //- length (in bytes) of block USERTEXT
  long	reserved_res_desc1;		  //- RESERVED
//- Arrays :
  long	trigtime_array;         //- length (in bytes) of TRIGTIME
  long	ris_time_array;         //- length (in bytes) of RIS_TIME
  long	res_array1;             //- RESERVED
  long	wave_array_1;           //- length (in bytes) of 1st simple data array
  long	wave_array_2;           //- length (in bytes) of 2nd simple data array
//- Instrument identification : NOT USED
  char	reserved_1[48];         //- RESERVED
//- Waveform description and time at which the waveform was generated
  long	wave_array_count;       //- nb of data points in the data array
  long	points_per_screen;      //- nominal number of data points on the screen
  long	res_first_pnt_val;      //- RESERVED
  long	res_last_pnt_val;       //- RESERVED
  long	first_point;            //- indicates the offset relative to the beginning of the trace buffer
  char	reserved_2[20];         //- RESERVED
  float	vertical_gain;          //-
  float	vertical_offset;        //- to get floating value from raw data : V_gain * data - V_offset
  char	reserved_3[8];          //- RESERVED
  short	nominal_bits;           //- intrinsic presision
  char	reserved_4[2];          //- RESERVED
  float	horizontal_interval;    //- sampling interval for time domain waveforms
  double	horizontal_offset;    //- trigger offset for the first sweep of the trigger, seconds between
                                                        //- the trigger and the first data point
  double	pixel_offset;         //- needed to know how to display the waveform
  char	vertical_unit[48];      //- units of the vertical axis
  char	horizontal_unit[48];    //- units of the horizontal axis
  float	reserved_5;
//- Trigger infos
  double	trigger_time_seconds; //- time (in sec) of the trigger
  char	trigger_time_minutes;   //- time (in min) of the trigger
  char	trigger_time_hours;     //- time (in hours) of the trigger
  char	trigger_time_days;      //- day of the trigger
  char	trigger_time_months;    //- month of the trigger
  short	trigger_time_year;      //- year of the trigger
  short	trigger_time_unused;    //- RESERVED
  float	acq_duration;           //- duration of the acquisition (in sec) in multi-trigger waveforms
  char	reserved_6[30];         //- RESERVED for the moment
  //- TODO :  timebase, ... till wavesource (from the lecroy doc).
} WAVEDESC_BLOCK;

//- RESTORE DEFAULT ALIGNEMENT
#pragma pack()

class WaveForm_data
{
private:

  char ptrRawData[MAX_WAVEFORM_DATA_LENGTH];        //- ptr on the received waveform data

  SocketLecroy*   ptr_com;

  //- Waveform description :
  WAVEDESC_BLOCK *waveBlockData;                    //- ptr on the struct WAVEDESC_BLOCK

  //- time info to acquire waveform data
  std::string time_to_get_waveformData;

  void get_waveform_data (ChannelData* channelData); //- acquire the waveform data from the scope

  void get_trigger_time_value (ChannelData* channelData);
    
public:

  //- CTOR
  WaveForm_data(char* lecroyIpAddress);
  //- DTOR
  ~WaveForm_data();

  void update_channel_data(ChannelData*);

};

#endif //- _WaveForm_data_H
