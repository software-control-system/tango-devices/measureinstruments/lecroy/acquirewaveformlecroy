//*******************************************************************************
//* Copyright (c) 2008-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
//******************************************************************************************
//
//
//		september 13, 2004 :  Header file for the communication in socket mode
//
//								with a Lecroy scope (avaiable for all models)
//
//
//		author : X.Elattaoui
//
//		SocketLecroy.h: interface for the socket class.
//
//******************************************************************************************

#ifndef _SocketLecroy_H
#define _SocketLecroy_H

//- necessary includes
#ifdef WIN32
#include <winsock2.h>
#endif
#include "SocketException.h"
////////////////////////////////////////////////////////////////////
//
//		SocketLecroy :: DEFINITION
//
////////////////////////////////////////////////////////////////////

//- Structure of the command Header
typedef struct 
{
	unsigned char 	bEOI_Flag;
	unsigned char	reserved[3];
	int				iLength;
} TCP_HEADER;

 //-	Constants definitions 
 //-----------------------------------------------
const int SERVER_PORT		= 1861;			//- It's the same port number for all Lecroy scope's.
const int EOI_FLAG			= 0x01;
const int SRQ_FLAG			= 0x08;
const int CLEAR_FLAG		= 0x10;
const int LOCKOUT_FLAG	= 0x20;
const int REMOTE_FLAG		= 0x40;
const int DATA_FLAG			= 0x80;

const unsigned int TCP_MINIMUM_PACKET_SIZE = 64;

class SocketLecroy  
{
private:

	SocketLecroy();											//- just one instance of SocketLecroy must be done for all waveforms of a same Lecroy device
	~SocketLecroy();

	static SocketLecroy* SL_instance;		//- ptr on the SocketLecroy instance

	static bool sConnectedFlag;

public:
	
	static SocketLecroy* get_instance(void);
	static void delete_instance();
	
	bool is_connected() {
	  return sConnectedFlag;
	}
	
	void write_read			(char* in_buf, unsigned int in_length, char* out_buf, int* out_length, bool eoi_flag);
	
	void TCP_WriteDevice	(char *buf, int length,bool eoi_flag);										//- send a cmd to the device
	void TCP_ReadDevice		(char *buf, unsigned int length, int* nb_byte_received);	//- read the device replie
	void TCP_Connect		(char *ip_address);																					//- build TCP/IP connection
	void TCP_Disconnect		(void);																										//- disconnect the device
	void TCP_ClearDevice	(void);																										//- disconnect and reconnect the device

};

#endif // _SocketLecroy_H
