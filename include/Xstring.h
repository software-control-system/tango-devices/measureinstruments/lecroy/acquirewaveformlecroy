//*******************************************************************************
//* Copyright (c) 2008-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
#ifndef XSTRING_H
#define XSTRING_H

#include <sstream>
#include <string>

template<class T>   class XString{

public:	
	
	inline static	T convertFromString(const std::string& s)
	{
		std::istringstream in(s);
		T x;
		if (in >> x)
			return x;
		// some sort of error handling goes here...
		return 0;
	} 
//
	inline static	std::string convertToString(const T & t)
	{
		std::ostringstream out ;
		if (out << t   )
			return out.str();
		// some sort of error handling goes here...
		return 0;
	} 

};
#endif
