// ============================================================================
//
// = CONTEXT
//    WaveformMgr
//
// = File
//    WaveformMgr.hpp
//
// = AUTHOR
//    X. Elattaoui Synchrotron Soleil France
//
// ============================================================================

#ifndef _WAVEFORM_MGR_H
#define _WAVEFORM_MGR_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <string>
#include "ChannelData.hpp"
#include "Waveform.h"
#include <yat4tango/DeviceTask.h>
#include "SocketException.h"

/**
*  WaveformMgr class description:
*    This class updates channels data.
*/

// ============================================================================
// class: WaveformMgr
// ============================================================================
class WaveformMgr : public yat4tango::DeviceTask 
{
public:
  //- ctor ---------------------------------
  WaveformMgr(Tango::DeviceImpl * host_device, char* lecroyIPAddress);
  
  //- dtor ---------------------------------
  ~WaveformMgr();
  
  //- add a channel to gather data
  void add_channel(std::string);
  
  //- getters ------------------------------
  ChannelData* get_channel_data();
  
  //- Lecroy direct communication
  std::string write_read(std::string);
  
  //- check socket is opened
  bool is_connected() {
	 return connected_;
  }
  
  std::string get_errors() {
	 yat::AutoMutex<> guard(this->error_mutex_);
	 return errors_;
  }
  
protected:
    
  //- process_message (implements yat4tango::DeviceTask pure virtual method)
  virtual void process_message (yat::Message& msg);
  
  //- internal mutex
  yat::Mutex  data_mutex_;
  yat::Mutex  error_mutex_;
    
private:
  
  //- waveform data access
  WaveForm_data*  waveform_ptr_;
  
  //- channel name and its VALUEs
  ChannelData* channel_data_;
  
  //- Lecroy Write Read response
  std::string lecroy_resp_;
  
  //- Lecroy IP address to connect to
  char* ipAddress_;
  
  //- lecroy socket connection
  bool connected_;
  
  //- error handling
  std::string errors_;
  
  //- Method to convert all lecroy exceptions (type Waveform or Socket exceptions) on Tango exception
  Tango::DevFailed lecroy_to_tango_exception(const lecroy::LecroyException& de);
  
};

#endif //- _WAVEFORM_MGR_H
