//*******************************************************************************
//* Copyright (c) 2008-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
//******************************************************************************************
//
//
//		september 13, 2004 :  Header file for the waveform exceptions in socket mode
//
//								with a Lecroy scope (avaiable for all models)
//
//
//		author : X.Elattaoui
//
//		WaveformException.h: interface for the socket exception class.
//
//******************************************************************************************

#ifndef _WAVEFORM_EXCEPTION_H_
#define _WAVEFORM_EXCEPTION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "LecroyException.h"

namespace lecroy 
{
// ============================================================================
//! Waveform exception class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class WaveformException : public lecroy::LecroyException
{
public:

  /**
   * TODO: remove this constructor
   */
  WaveformException (void);

  /**
   * TODO: remove this constructor
   */
  WaveformException (const char *reason,
					           const char *desc,
					           const char *origin,
                     int severity = lecroy::ERR);

  /**
   * Initialization. 
   */
  WaveformException (const std::string& reason,
					           const std::string& desc,
					           const std::string& origin,
                     int severity = lecroy::ERR);
  
  /**
   * Copy constructor 
   */
  WaveformException(const WaveformException& src);
  /**
   * Release resources.
   */
  virtual ~WaveformException (void);

  /**
   * operator=
   */
  WaveformException& operator= (const WaveformException& _src); 
};

} // end namespace

#endif // _WAVEFORM_EXCEPTION_H_
