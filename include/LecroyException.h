//*******************************************************************************
//* Copyright (c) 2008-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
#ifndef _LECROYEXCEPTION
#define _LECROYEXCEPTION

#include <string>
#include <vector>

namespace lecroy{

// ============================================================================
// lecroy Errors severities 
// ============================================================================
typedef enum {
  WARN, 
  ERR, 
  PANIC
} ErrorSeverity;

// ============================================================================
//! The lecroy exception abstraction base class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class Error
{
public:

  /**
   * Initialization. 
   */
  Error (void);

  /**
   * Initialization. 
   */
  Error (const char *reason,
				 const char *desc,
				 const char *origin,
	       int severity = lecroy::ERR);
  

  /**
   * Initialization. 
   */
  Error (const std::string& reason,
				 const std::string& desc,
				 const std::string& origin, 
	       int severity = lecroy::ERR);

  /**
   * Copy constructor. 
   */
  Error (const Error& src);

  /**
   * Error details: code 
   */
  virtual ~Error (void);

  /**
   * operator= 
   */
  Error& operator= (const Error& _src);

  /**
   * Error details: reason 
   */
  std::string reason;

  /**
   * Error details: description 
   */
  std::string desc;

  /**
   * Error details: origin 
   */
  std::string origin;

  /**
   * Error details: severity 
   */
  int severity;

};

// ============================================================================
// The lecroy error list.	
// ============================================================================
typedef std::vector<Error> ErrorList;

// ============================================================================
//! The lecroy exception abstraction base class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class  LecroyException
{
public:

  /**
   * Initialization. 
   */
  LecroyException (void);

  /**
   * Initialization. 
   */
  LecroyException (const char *reason,
					   const char *desc,
					   const char *origin,
	           int severity = lecroy::ERR);
  
  /**
   * Initialization. 
   */
  LecroyException (const std::string& reason,
					   const std::string& desc,
					   const std::string& origin, 
	           int severity = lecroy::ERR);

  /**
   * Initialization. 
   */
  LecroyException (const Error& error);


  /**
   * Copy constructor. 
   */
  LecroyException (const LecroyException& src);

  /**
   * operator=
   */
  LecroyException& operator= (const LecroyException& _src); 

  /**
   * Release resources.
   */
  virtual ~LecroyException (void);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const char *reason,
					         const char *desc,
						       const char *origin, 
		               int severity = lecroy::ERR);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const std::string& reason,
                   const std::string& desc,
                   const std::string& origin, 
                   int severity = lecroy::ERR);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const Error& error);

  /**
   * The errors list
   */
   ErrorList errors;

};

} // end namspace lecroy
#endif //__LECROYEXCEPTION
