//*******************************************************************************
//* Copyright (c) 2008-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
//******************************************************************************************
//
//
//		september 13, 2004 :  Header file for the sockets exceptions in socket mode
//
//								with a Lecroy scope (avaiable for all models)
//
//
//		author : X.Elattaoui
//
//		SocketException.h: interface for the socket exception class.
//
//******************************************************************************************

#ifndef _SOCKET_EXCEPTION_H_
#define _SOCKET_EXCEPTION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "LecroyException.h"

namespace lecroy 
{
// ============================================================================
//! Socket exception class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class SocketException : public lecroy::LecroyException
{
public:

  /**
   * TODO: remove this constructor
   */
  SocketException (void);

  /**
   * TODO: remove this constructor
   */
  SocketException (const char *reason,
					         const char *desc,
					         const char *origin,
                   int severity = lecroy::ERR);

  /**
   * Initialization. 
   */
  SocketException (const std::string& reason,
					         const std::string& desc,
					         const std::string& origin,
                   int severity = lecroy::ERR);
  
  /**
   * Copy constructor 
   */
  SocketException(const SocketException& src);

  /**
   * Release resources.
   */
  virtual ~SocketException (void);

  /**
   * operator=
   */
  SocketException& operator= (const SocketException& _src); 
};

} // end namespace

#endif // _SOCKET_EXCEPTION_H_
