//*******************************************************************************
//* Copyright (c) 2008-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
//******************************************************************************************
//
//
//		september 13, 2004 :  Source file for the socket exceptions in socket mode
//
//								with a Lecroy scope (avaiable for all models)
//
//
//		author : X.Elattaoui
//
//		SocketException.cpp: interface for the socket exception class.
//
//******************************************************************************************

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "SocketException.h"

namespace lecroy 
{
// ============================================================================
// SocketException::SocketException
// ============================================================================
SocketException::SocketException (void)
 : LecroyException()
{

}

// ============================================================================
// SocketException::SocketException
// ============================================================================
SocketException::SocketException (const char *_reason,
					                        const char *_desc,
					                        const char *_origin,
									                int _severity)
 : LecroyException(_reason, _desc, _origin, _severity)
{

}

// ============================================================================
// SocketException::SocketException
// ============================================================================
SocketException::SocketException (const std::string& _reason,
					                        const std::string& _desc,
					                        const std::string& _origin,
                                  int _severity)
 : LecroyException(_reason, _desc, _origin, _severity)
{

}
  
// ============================================================================
// SocketException::SocketException
// ============================================================================
SocketException::SocketException (const SocketException& _src)
  : LecroyException(_src)
{

}

// ============================================================================
// SocketException::~SocketException
// ============================================================================
SocketException::~SocketException (void)
{

}

// ============================================================================
// SocketException::SocketException
// ============================================================================
SocketException& SocketException::operator= (const SocketException& _src) 
{
  LecroyException::operator=(_src);
  return *this;
}

} // namespace lecroy
