// ============================================================================
//
// = CONTEXT
//    TANGO Project - Task to gather Lecroy channel(s) data
//
// = File
//    WaveformMgr.cpp
//
// = AUTHOR
//    X. Elattaoui - SOLEIL
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/time/Time.h>
#include "WaveformMgr.hpp"
#include "SocketLecroy.h"		//- write/read

const std::size_t MAX_RESPONSE_LENGTH  = 150000;

const std::size_t PERIODIC_MSG_PERIOD  = 1100; //- (in ms) : time to update one channel data, it cannot be less than 1100 ms!

const std::size_t MSG_TIMEOUT		       = 2000; //- in ms
// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
const std::size_t WRITE_READ 		       = yat::FIRST_USER_MSG + 10;

// ======================================================================
// WaveformMgr::WaveformMgr
// ======================================================================
WaveformMgr::WaveformMgr (Tango::DeviceImpl* host_device, char* ipAdd)
  : yat4tango::DeviceTask(host_device),
    waveform_ptr_(0),
    channel_data_(0),
    lecroy_resp_(""),
    ipAddress_(ipAdd),
    connected_(false),
    errors_("")
{
  //- noop
}

// ======================================================================
// WaveformMgr::~WaveformMgr
// ======================================================================
WaveformMgr::~WaveformMgr (void)
{
//   std::cout << "\tWaveformMgr::~WaveformMgr ..." << std::endl;
  //- noop
//   std::cout << "\tWaveformMgr::~WaveformMgr -> DONE" << std::endl;
}

// ============================================================================
// WaveformMgr::process_message
// ============================================================================
void WaveformMgr::process_message (yat::Message& _msg)
{
  DEBUG_STREAM << "WaveformMgr::handle_message::receiving msg " << _msg.to_string() << std::endl;
  
  //- handle msg
  switch (_msg.type())
  {
    //- THREAD_INIT ----------------------
    case yat::TASK_INIT:
    {
      DEBUG_STREAM << "WaveformMgr::handle_message::THREAD_INIT::thread is starting up" << std::endl;   
      //- "initialization" code goes here
      //----------------------------------------------------
      yat::Timer t;
      
  	  if ( !waveform_ptr_ )
      {
  		  waveform_ptr_ = new WaveForm_data(ipAddress_);
      }

  	  if (waveform_ptr_)
  	  {
    		//- check socket is up
    		connected_ = SocketLecroy::get_instance()->is_connected();
    		//- configure optional msg handling
    		this->enable_timeout_msg(false);
    		this->enable_periodic_msg(true);
     		this->set_periodic_msg_period(PERIODIC_MSG_PERIOD);
    		this->errors_.clear();
    		INFO_STREAM << "\tWaveformMgr::TASK_INIT finished in " << t.elapsed_msec() << " ms." << std::endl;
  	  }
  	  else
  	  {
    		yat::AutoMutex<> guard(this->error_mutex_);
    		errors_ = "Manager : failed to create channel structure to update data.";
  	  }
    }
    break;
    //- THREAD_EXIT ----------------------
    case yat::TASK_EXIT:
    {
      DEBUG_STREAM << "WaveformMgr::handle_message::THREAD_EXIT::thread is quitting" << std::endl;
      //- "release" code goes here
      //----------------------------------------------------
  	  if ( waveform_ptr_ )
  	  {
    		delete waveform_ptr_;
    		waveform_ptr_ = 0;
  	  }
  	  yat::AutoMutex<> guard(this->data_mutex_);
  	  if ( channel_data_ )
  	  {
    		delete channel_data_;
    		channel_data_ = 0;
  	  }
    }
    break;
    //- THREAD_PERIODIC ------------------
    case yat::TASK_PERIODIC:
    {
      //- code relative to the task's periodic job goes here
      //----------------------------------------------------
      yat::Timer t;
      
      if (waveform_ptr_)
      {
        try
        {
    		  ChannelData tmp_channel(channel_data_->channel_name_);
    		  waveform_ptr_->update_channel_data(&tmp_channel);
    		  {//- critical section
      			yat::AutoMutex<> guard(this->data_mutex_);
      			*channel_data_ = tmp_channel;
    		  }
    		  {//- no error
      			yat::AutoMutex<> guard(this->error_mutex_);
      			errors_.clear();
    		  }
          INFO_STREAM << "\tWaveformMgr::TASK_PERIODIC -> done in " << t.elapsed_msec() << " ms." << std::endl;
        }
        catch(lecroy::SocketException& se)
        {
          ERROR_STREAM << "[...] WaveformMgr::handle_message::handling THREAD_PERIODIC -> [SocketException]" << std::endl;            
          Tango::DevFailed df = lecroy_to_tango_exception(se);
          ERROR_STREAM << df << std::endl;
          {
            yat::AutoMutex<> guard(this->error_mutex_);
            errors_ = "Manager : failed to update channel structure to update data.\n Caught a lecroy::SocketException exception.";
          }
          throw df;
        }
        catch(const lecroy::WaveformException& we)
        {
          Tango::DevFailed df = lecroy_to_tango_exception(we);
          ERROR_STREAM << df << std::endl;
    		  {
      			yat::AutoMutex<> guard(this->error_mutex_);
      			errors_ = "Manager : failed to update channel structure to update data.\n Caught a lecroy::WaveformException exception -> check logs.";
    		  }
          throw df;
        }
        catch(Tango::DevFailed& df)
        {
          ERROR_STREAM << "[DF] WaveformMgr::handle_message::handling THREAD_PERIODIC -> caught DF :\n" << df << std::endl;            
    		  {
      			yat::AutoMutex<> guard(this->error_mutex_);
      			errors_ = "Manager : failed to update channel structure to update data.\n Caught a DevFailed exception -> check logs.";
    		  }
          throw df;
        }
        catch(...)
        {
          ERROR_STREAM << "[...] WaveformMgr::handle_message::handling THREAD_PERIODIC -> [...]" << std::endl;            
          {
            yat::AutoMutex<> guard(this->error_mutex_);
            errors_ = "Manager : failed to update channel structure to update data.\n Caught a generic exception.";
          }
          throw;
        }
      }
      else
  	  {
        ERROR_STREAM << "[ELSE] WaveformMgr::handle_message::handling THREAD_PERIODIC -> Cannot update channel data" << std::endl;
    		yat::AutoMutex<> guard(this->error_mutex_);
    		errors_ = "Failed to create channel structure to update data.";
  	  }
    }
    break;
	//- WRITE_READ
	case WRITE_READ:
    {
      //- code relative to the task's STORE_DATA handling goes here
      YAT_LOG("CurrentTrendFileTask::handle_message::handling WRITE_READ msg");
      yat::Timer t;
	  
  	  try
  	  {
    		SocketLecroy* ptr_com = SocketLecroy::get_instance();
    		
    		std::string cmd = _msg.get_data<std::string>();
    		int bytes_received = -1;
    		char resp[MAX_RESPONSE_LENGTH];
    		char * command = 0;
    		
    		if(ptr_com)
    		{
    		  ::strcpy(resp, "No response");
    		  //- transform string to char*
    		  std::size_t length = cmd.size();
    		  command = new char[length+1];
    		  for(std::size_t i=0; i<length; i++)
    			command[i] = cmd[i];
    		  command[length] = '\0';

    		  ptr_com->TCP_WriteDevice(command, length, true);
    		  if( cmd.rfind('?') != std::string::npos )
    		  {
#ifdef WIN32
	Sleep(10); //- milliseconds
#else
	usleep(10000);
#endif
            ptr_com->TCP_ReadDevice(resp, MAX_RESPONSE_LENGTH, &bytes_received);
    		  }
    		}
    		else
        {
    		  ::strcpy(resp, "No communication with Lecroy device!");
        }
    		
    		if (command)
    		{
    		  delete [] command;
    		  command = 0;
    		}

    		this->lecroy_resp_ = resp;
    		{//- no error
    		  yat::AutoMutex<> guard(this->error_mutex_);
    		  errors_.clear();
    		}
  		  INFO_STREAM << "\t******WaveformMgr::WRITE_READ done in : " << t.elapsed_msec() << " ms.\n" << std::endl;
  	  }
      catch(const lecroy::WaveformException& we)
      {
        Tango::DevFailed df = lecroy_to_tango_exception(we);
        FATAL_STREAM << df << std::endl;
        {
          yat::AutoMutex<> guard(this->error_mutex_);
          errors_ = "WriteRead command failed : caught a Lecroy exception -> check logs.";
        }
      }
  	  catch(Tango::DevFailed& df)
  	  {
    		ERROR_STREAM << "WaveformMgr::handle_message::handling WRITE_READ -> caught DF :\n" << df << std::endl;            
    		{
    		  yat::AutoMutex<> guard(this->error_mutex_);
    		  errors_ = "WriteRead command failed : caught a DevFailed exception -> check logs.";
    		}
  	  }
  	  catch(...)
  	  {
    		ERROR_STREAM << "WaveformMgr::handle_message::handling WRITE_READ -> [...]" << std::endl;            
    		{
    		  yat::AutoMutex<> guard(this->error_mutex_);
    		  errors_ = "WriteRead command failed : caught a generic exception.";
    		}
    	}
    }
    break;
    //- THREAD_TIMEOUT -------------------
    case yat::TASK_TIMEOUT:
    {
      //- code relative to the task's tmo handling goes here    
    }
    break;
    //- UNHANDLED MSG --------------------
    default:
      FATAL_STREAM << "WaveformMgr::handle_message::unhandled msg type received" << std::endl;
      break;
  }
  
  DEBUG_STREAM  << "WaveformMgr::handle_message::message_handler:msg "
                << _msg.to_string()
                << " successfully handled"
                << std::endl;
}

// ============================================================================
// WaveformMgr::add_channel
// ============================================================================
void WaveformMgr::add_channel(std::string ch_name)
{
  //- insert the new channel (in critical section)
  yat::AutoMutex<> guard(this->data_mutex_);
  if ( !channel_data_ )
  {
	 channel_data_ = new ChannelData(ch_name);
  }
 
  //- allocate data memory
  channel_data_->vertical_scaled_data_.reserve(MAX_WAVEFORM_DATA_LENGTH);
  channel_data_->raw_waveform_data_.reserve(MAX_WAVEFORM_DATA_LENGTH);
}

// ============================================================================
// WaveformMgr::get_channel_data
// ============================================================================
ChannelData* WaveformMgr::get_channel_data()
{
  yat::AutoMutex<> guard(this->data_mutex_);

  return channel_data_;
}

// ============================================================================
// WaveformMgr::write_read
// ============================================================================
std::string WaveformMgr::write_read(std::string cmd_to_send)
{
  //- in case you want to wait for the msg to be handled before returning (synchronous approach)
  //- be sure to allocate a "waitable" message...
  bool waitable = true;
  yat::Message * msg = new yat::Message(WRITE_READ, MAX_USER_PRIORITY, waitable);

  msg->attach_data(cmd_to_send);

  this->wait_msg_handled(msg, MSG_TIMEOUT);
  
  return lecroy_resp_;
}

//+----------------------------------------------------------------------------
//
// method : WaveformMgr::lecroy_to_tango_exception()
//
// description :  convert Lecroy exception to a DevFailed.
//
//-----------------------------------------------------------------------------
Tango::DevFailed WaveformMgr::lecroy_to_tango_exception(const lecroy::LecroyException& de)
{
  Tango::DevErrorList error_list(de.errors.size());
  error_list.length(de.errors.size());

  for(size_t i = 0; i < de.errors.size(); i++)
  {
    error_list[i].reason = CORBA::string_dup(de.errors[i].reason.c_str());
    error_list[i].desc   = CORBA::string_dup(de.errors[i].desc.c_str());
    error_list[i].origin = CORBA::string_dup(de.errors[i].origin.c_str());

    switch(de.errors[i].severity)
    {
    case lecroy::WARN:
      error_list[i].severity = Tango::WARN;
      break;
    case lecroy::PANIC:
      error_list[i].severity = Tango::PANIC;
      break;
    case lecroy::ERR:
    default:
      error_list[i].severity = Tango::ERR;
      break;
    }
  }

  return Tango::DevFailed(error_list);
}
