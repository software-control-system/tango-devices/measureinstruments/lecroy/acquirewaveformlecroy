//*******************************************************************************
//* Copyright (c) 2004-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
//******************************************************************************************
//
//
//		september 13, 2004 :  Source file for the waveforms exceptions in socket mode
//
//								with a Lecroy scope (avaiable for all models)
//
//
//		author : X.Elattaoui
//
//		WaveformException.cpp: interface for the waveform exception class.
//
//******************************************************************************************

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "WaveformException.h"

namespace lecroy 
{
// ============================================================================
// WaveformException::WaveformException
// ============================================================================
WaveformException::WaveformException (void)
 : LecroyException()
{

}

// ============================================================================
// WaveformException::WaveformException
// ============================================================================
WaveformException::WaveformException (const char *_reason,
					                  const char *_desc,
					                  const char *_origin,
                            int _severity)
 : LecroyException(_reason, _desc, _origin, _severity)
{

}

// ============================================================================
// WaveformException::WaveformException
// ============================================================================
WaveformException::WaveformException (const std::string& _reason,
					                  const std::string& _desc,
					                  const std::string& _origin,
                            int _severity)
 : LecroyException(_reason, _desc, _origin, _severity)
{

}
  
// ============================================================================
// WaveformException::WaveformException
// ============================================================================
WaveformException::WaveformException (const WaveformException& _src)
  : LecroyException(_src)
{

}

// ============================================================================
// WaveformException::~WaveformException
// ============================================================================
WaveformException::~WaveformException (void)
{

}

// ============================================================================
// WaveformException::WaveformException
// ============================================================================
WaveformException& WaveformException::operator= (const WaveformException& _src) 
{
  LecroyException::operator=(_src);
  return *this;
}

} // namespace lecroy
