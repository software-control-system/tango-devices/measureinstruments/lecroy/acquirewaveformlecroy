//*******************************************************************************
//* Copyright (c) 2008-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
//******************************************************************************************
//
//
//		september 13, 2004 :  Source file for the communication exceptions in socket mode
//
//								with a Lecroy scope (avaiable for all models)
//
//
//		author : X.Elattaoui
//
//		LecroyException.cpp: interface for the socket & waveform exceptions class.
//
//******************************************************************************************

// ============================================================================
// DEPENDENCIES
#include "LecroyException.h"

namespace lecroy 
{
// ============================================================================
// Error::Error
// ============================================================================
Error::Error (void)
  :  reason ("unknown"),
     desc   ("unknown error"),
     origin ("unknown"),
     severity (lecroy::ERR)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const char *_reason,
	      const char *_desc,
              const char *_origin,
              int _severity)
  :  reason (_reason),
     desc   (_desc),
     origin (_origin),
     severity (_severity)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const std::string& _reason,
	            const std::string& _desc,
	            const std::string& _origin,
              int _severity)
  :  reason (_reason),
     desc (_desc),
     origin (_origin),
     severity (_severity)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const Error& _src)
  :  reason (_src.reason),
     desc   (_src.desc),
     origin (_src.origin),
     severity (_src.severity)
{

}

// ============================================================================
// Error::~Error
// ============================================================================
Error::~Error (void)
{

}

// ============================================================================
// Error::operator=
// ============================================================================
Error& Error::operator= (const Error& _src) 
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }

  this->reason	= _src.reason;
  this->desc	= _src.desc;
  this->origin	= _src.origin;
  this->severity = _src.severity;

  return *this;
}

// ============================================================================
// LecroyException::LecroyException
// ============================================================================
LecroyException::LecroyException (void) 
  : errors(0)
{
  this->push_error(Error());
}

// ============================================================================
// LecroyException::LecroyException
// ============================================================================
LecroyException::LecroyException (const char *_reason,
                                  const char *_desc,
                                  const char *_origin,
                                  int _severity) 
  : errors(0)
{
  this->push_error(Error(_reason, _desc, _origin, _severity));
}

// ============================================================================
// LecroyException::LecroyException
// ============================================================================
LecroyException::LecroyException (const std::string& _reason,
                                  const std::string& _desc,
                                  const std::string& _origin,
                                  int _severity) 
  : errors(0)
{
  this->push_error(_reason, _desc, _origin,_severity);
}

// ============================================================================
// LecroyException::LecroyException
// ============================================================================
LecroyException::LecroyException (const LecroyException& _src) 
  : errors(0)
{
  for (unsigned int i = 0; i < _src.errors.size();  i++) {
    this->push_error(_src.errors[i]);
  }
}

// ============================================================================
// LecroyException::LecroyException
// ============================================================================
LecroyException& LecroyException::operator= (const LecroyException& _src) 
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }

  this->errors.clear();

  for (unsigned int i = 0; i < _src.errors.size();  i++) {
    this->push_error(_src.errors[i]);
  }

  return *this;
}

// ============================================================================
// LecroyException::~LecroyException
// ============================================================================
LecroyException::~LecroyException (void)
{
  this->errors.clear();
}

// ============================================================================
// LecroyException::push_error
// ============================================================================
void LecroyException::push_error (const char *_reason,
    					                    const char *_desc,
					                        const char *_origin, 
                                  int _severity)
{
  this->errors.push_back(Error(_reason, _desc, _origin, _severity));
}

// ============================================================================
// LecroyException::push_error
// ============================================================================
void LecroyException::push_error (const std::string& _reason,
    					              const std::string& _desc,
					                  const std::string& _origin, 
                            int _severity)
{
  this->errors.push_back(Error(_reason, _desc, _origin, _severity));
}

// ============================================================================
// LecroyException::push_error
// ============================================================================
void LecroyException::push_error (const Error& _error)
{
  this->errors.push_back(_error);
}

} // namespace lecroy
